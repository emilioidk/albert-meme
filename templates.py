"""
Module for templates definitions and operations.
"""
import os
import sqlite3

module_path = os.path.realpath(os.path.dirname(__file__))
index = module_path + "/images/index.db"

con = sqlite3.connect(index, check_same_thread=False)
cursor = con.cursor()


def get_template(name_id):
    """
    Gets a a template by it's name ID. If the ID does not match to any existing template it returns None.
    """
    cursor.execute('SELECT * FROM images WHERE name_id= ?', (name_id,))
    return cursor.fetchone()


def search_template(search_term):
    """
    Returns a list of templates that follow the search criteria:
    * The name contains the search term as a substring
    * Any of the tags contain any of the words in the search term as a substring
    """
    base_query = 'SELECT * FROM images WHERE name LIKE ?'
    base_parameters = ('%{}%'.format(search_term),)

    individual_terms = search_term.split(' ')
    for term in individual_terms:
        base_query += ' OR tags LIKE ?'
        base_parameters += ('%{}%'.format(term),)

    cursor.execute(base_query, base_parameters)
    return cursor.fetchall()


def get_templates_name_ids():
    """
    Gets all the name IDs we have registered.
    """
    cursor.execute('SELECT name_id FROM images')
    all_templates = cursor.fetchall()
    return [template[0] for template in all_templates]
