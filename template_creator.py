"""
A template will have the following

* id: unique identifier (e.g. 1234)
* name: a pretty name for the themplate (e.g. "Awkward Penguin")
* name_id: a dash-seperated name (e.g. "awkward-penguin")
* tags: list of tags to describe and identify a template (e.g. ["fail", "awkward", "penguin"])
* inputs: list of inputs to fill in the template. Each input has the following fields
* * text_font: name of font to use for filling in the text (e.g. "impact")
* * text_font_size: default size of the font to use (e.g. 56)
* * text_font_size_min: minimum size of the font to use (e.g. 10)
* * text_name: name of the text in the template (e.g. "top")
* * text_color: tuple (R,G,B) representation of the color the text will have on the image (e.g. (0,0,0))
* * pos_x: x position of the text in the template (e.g. 0)
* * pos_y: y position of the text in the template (e.g. 0)
* * text_width: max with the text widget can have (e.g. 500)
* * text_height: max height the text can have (e.g. 100)
* * max_lines: max amount of lines the text can have (e.g. 2)
* file_path: absolute?relative? path to the image template
* thumbnail_path: full path to the image template thumbnail
* format: file format for the image template file

TODO: figure out proper thumbnail size
"""
