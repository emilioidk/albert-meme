"""
Functionality for adding text to images. This module is heavily based on https://github.com/lipsumar/meme-caption
"""
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from PIL import ImageSequence


def drawText(img, draw, msg, pos):
    lines = []

    fontSize = 56
    font = ImageFont.truetype("/home/emilio/.local/share/albert/org.albert.extension.python/modules/albert-meme/fonts/impact.ttf", fontSize)

    w, h = draw.textsize(msg, font)

    imgWidthWithPadding = img.width * 0.99

    # 1. how many lines for the msg to fit ?
    lineCount = 1
    if(w > imgWidthWithPadding):
        lineCount = int(round((w / imgWidthWithPadding) + 1))

    # If there are over 2 lines make font smaller until we have
    # up to 2 lines or font is too small
    if lineCount > 2:
        while 1:
            fontSize -= 2
            font = ImageFont.truetype("/home/emilio/.local/share/albert/org.albert.extension.python/modules/albert-meme/fonts/impact.ttf", fontSize)
            w, h = draw.textsize(msg, font)
            lineCount = int(round((w / imgWidthWithPadding) + 1))
            if lineCount < 3 or fontSize < 10:
                break

    # 2. divide text in X lines
    lastCut = 0
    isLast = False
    for i in range(0, lineCount):
        if lastCut == 0:
            cut = (len(msg) / lineCount) * i
        else:
            cut = lastCut

        if i < lineCount-1:
            nextCut = (len(msg) / lineCount) * (i+1)
        else:
            nextCut = len(msg)
            isLast = True

        # make sure we don't cut words in half
        if nextCut == len(msg) or msg[int(nextCut)] == " ":
            pass
        else:
            while int(nextCut) < len(msg) and msg[int(nextCut)] != " ":
                nextCut += 1

        line = msg[int(cut):int(nextCut)].strip()

        # is line still fitting ?
        w, h = draw.textsize(line, font)
        if not isLast and w > imgWidthWithPadding:
            nextCut -= 1
            while msg[int(nextCut)] != " ":
                nextCut -= 1

        lastCut = nextCut
        lines.append(msg[int(cut):int(nextCut)].strip())

    # 3. print each line centered
    lastY = -h
    if pos == "bottom":
        lastY = img.height - h * (lineCount+1) - 10

    for i in range(0, lineCount):
        w, h = draw.textsize(lines[i], font)
        textX = img.width/2 - w/2
        # if pos == "top":
        #    textY = h * i
        # else:
        #    textY = img.height - h * i
        textY = lastY + h
        draw.text((textX-2, textY-2), lines[i], fill=(0, 0, 0), font=font)
        draw.text((textX+2, textY-2), lines[i], fill=(0, 0, 0), font=font)
        draw.text((textX+2, textY+2), lines[i], fill=(0, 0, 0), font=font)
        draw.text((textX-2, textY+2), lines[i], fill=(0, 0, 0), font=font)
        draw.text((textX, textY), lines[i], fill=(255, 255, 255), font=font)
        lastY = textY

    return


def caption_gif(input_path, top_text, bottom_text, output_path):
    """
    Based on thread from GitHub issue https://github.com/python-pillow/Pillow/issues/3128
    """
    img = Image.open(input_path)

    # A list of the frames to be outputted
    frames = []
    # Loop over each frame in the animated image
    for frame in ImageSequence.Iterator(img):
        # Draw the text on the frame
        frame = frame.convert('RGBA')

        draw = ImageDraw.Draw(frame)

        drawText(frame, draw, top_text, "top")
        drawText(frame, draw, bottom_text, "bottom")
        del draw

        # Then append the single frame image to a list of frames
        frames.append(frame)

    # Save the frames as a new image
    frames[0].save(output_path, save_all=True, append_images=frames[1:])


def caption(input_path, top_text, bottom_text, output_path):
    img = Image.open(input_path).convert('RGB')
    draw = ImageDraw.Draw(img)

    drawText(img, draw, top_text, "top")
    drawText(img, draw, bottom_text, "bottom")

    img.save(output_path)
