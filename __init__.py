# -*- coding: utf-8 -*-

"""
Create memes from Albert. Supports both still images and gifs.

TODO: support thumbnails
TODO: companion system for administrating base memes
TODO: support creating memes from URLs
TODO: write synopsis
"""

from distutils.log import debug
from string import capwords
from albert import *
import os
from time import sleep
from pydoc import importfile
import subprocess
from datetime import datetime

__title__ = "Meme Generator"
__version__ = "0.4.2"
__triggers__ = "meme "
__authors__ = "Emilio Martin Lundgaard Lopez"
__exec_deps__ = ["xclip"]


module_path = os.path.realpath(os.path.dirname(__file__))
caption = importfile(module_path + "/caption.py")
templates = importfile(module_path + "/templates.py")

icon = module_path + "/icon.png"


def caption_and_copy(top_caption, bottom_caption, template):
    formatted_now = datetime.now().strftime('%Y%m%d%H%M%S')
    meme_path = '/home/emilio/Pictures/memes/albert/{}.jpg'.format(
        formatted_now)
    caption.caption(module_path+template[4],
                    top_caption, bottom_caption, meme_path)
    subprocess.check_call(
        ["xclip", "-selection", "clipboard", "-t", "image/png", meme_path])


def handleQuery(query):
    if not query.isTriggered:
        return

    first_term = query.string.split(' ')[0]
    debug('First term: "{}"'.format(first_term))

    matching_templates = templates.search_template(query.string)
    all_templates_name_ids = templates.get_templates_name_ids()

    if first_term in all_templates_name_ids:
        template = templates.get_template(first_term)
        captions = query.string[len(first_term):].split('|')
        top_caption = captions[0].strip()
        bottom_caption = captions[1].strip() if len(captions) > 1 else ""

        debug('TOP: {}'.format(top_caption))
        debug('BOTTOM: {}'.format(bottom_caption))
        return [Item(
            id=str(template[0]),
            icon=module_path + template[4],
            text=template[1],
            subtext=f"{__triggers__}{template[2]} UP {top_caption} | DOWN {bottom_caption}",
            completion='meme {} '.format(template[2]),
            actions=[
                FuncAction(
                    "Generate and copy to clipboard",
                    lambda top_caption=top_caption, bottom_caption=bottom_caption, template=template: caption_and_copy(
                        top_caption=top_caption, bottom_caption=bottom_caption, template=template)
                )
            ]
        )]

    return [Item(
        id=str(template[0]),
        icon=module_path + template[4],
        text=template[1],
        subtext=f"{__triggers__}{template[2]} UP TEXT | DOWN TEXT",
        completion='meme {} '.format(template[2]),
    ) for template in matching_templates]
